var express = require('express');
var app = express();
var secret = "secretSHHHH";
var passwordHash = require('password-hash');
var bodyParser = require('body-parser');
app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
}));
var payloadToken = null;
var payloadRefreshToken = null;
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

//conection with mongo
mongoose.connect('mongodb://localhost/usersOauth',function (err,res) {
    if(err) console.log('Fallo al conectar a la BD:' + err);
    else console.log('Todo OK');
});

//schema mongo
var userSchema = new Schema({
    id: String,
    name: String,
    surname: String,
    username: {type:String, unique:true},
    password: String,
    email: String,
    activaded: Boolean,
    roles: {String,String}
});
var userDao = mongoose.model('User', userSchema);
var jwt = require('jwt-simple');

app.use(passport.initialize());

//this findOne use passport to login the user.
function findOne(userName, password, done) {
    userDao.findOne({ username: userName }, function (err, user) {
        if (err) return done(null, false, { message: 'Fallo no controlado.' });

        if (!user) return done(null, false, { message: 'Usuario incorrecto.' });

        if (passwordHash.verify(password, user.password)) {
            return done(null, { username: userName, password: password });
        } else {
            return done(null, false, { message: 'Incorrect password.' });
        }
    })
};
//If the user is login,create token jwt using data of user
function createToken(req,res){
        //userDao.findOne({username:req.body.username},function(err,user){
        userDao.findOne({username:req.query.username},function(err,user){
        if(user){
            payloadToken = {
                id: user._id,
                name: user.name,
                password: user.password,
                surname: user.surname,
                username: user.username,
                roles: user.roles,
                email: user.email,
                exp: Date.now() + 1800000,
            }
            token = jwt.encode(payloadToken,secret);
            payloadRefreshToken = {
                exp: Date.now() + 1800000,
                token: token
            }
            refreshToken = jwt.encode(payloadRefreshToken,secret)
            payloadToken = {}
            payloadRefreshToken = {}
            res.send(token + "|" + refreshToken);
        }
    })
};
//this createToken create the new token when the user call to refresh token
function createTokenUsername(req,res,userName){
    userDao.findOne({username:userName},function(err,user){
        if(user){
            payloadToken = {
                id: user._id,
                name: user.name,
                password: user.password,
                surname: user.surname,
                username: user.username,
                roles: user.roles,
                email: user.email,
                exp: Date.now() + 1800000,
            }
            token = jwt.encode(payloadToken,secret);
            payloadRefreshToken = {
                exp: Date.now() + 1800000,
                token: token
            }
            refreshToken = jwt.encode(payloadRefreshToken,secret)
            payloadToken = {}
            payloadRefreshToken = {}
            res.send(token + "|" + refreshToken);
        }
    })
};

//this function verify the token
function verifyToken(req,res){
    if (!req.headers.authorization){
        return res.status(403).send({message:"No token"})
    }
    tokenClient = req.headers.authorization;    
    payloadToken = jwt.decode(tokenClient,secret);
    console.log(payloadToken.username)
    userDao.findOne({username:payloadToken.username},function(err,user){
        if(user){
            if (payloadToken.password === user.password){
                if(payloadToken.exp <= Date.now()){
                    return res.status(401).send({message:"Token date invalid"});
                }
            else{
                return res.send(payloadToken)
                }

            }else{
                return res.status(401).send({message:"Unauthorized"});
            }
        }
    });
};   

//the call of passport
passport.use(new LocalStrategy(
    function (username, password, done) {
        return findOne(username, password, done);
    }
));

app.get("/token-local", passport.authenticate('local', { session: false }), function (req, res) {
    createToken(req,res);
});

app.get("/verify-token", function (req,res) {
    verifyToken(req,res);
});

app.get("/refresh-token",function(req,res){
    if (!req.headers.authorization){
        return res.status(403).send({message:"No token"})
    }

    tokenClient = req.headers.authorization;
    payloadRefreshToken = jwt.decode(tokenClient,secret);
    if(payloadRefreshToken.exp <= Date.now()){
        return res.status(401).send({message:"Token date invalid"})
    }
    else{
        userName = jwt.decode(payloadRefreshToken.token,secret).username;
        createTokenUsername(req,res,userName);
    }
});
/**app.post("/token-local", passport.authenticate('local', { session: false }), function (req, res) {
    console.log("hola!!");
    createToken(req,res);
});

app.post("/verify-token", function (req,res) {
    verifyToken(req,res);
});

app.post("/refresh-token",function(req,res){
    if (!req.headers.authorization){
        return res.status(403).send({message:"No token"})
    }

    tokenClient = req.headers.authorization;
    payloadRefreshToken = jwt.decode(tokenClient,secret);
    if(payloadRefreshToken.exp <= Date.now()){
        return res.status(401).send({message:"Token date invalid"})
    }
    else{
        userName = jwt.decode(payloadRefreshToken.token,secret).username;
        createTokenUsername(req,res,userName);
    }
});**/

//port of app
app.listen(3000, function () {
});